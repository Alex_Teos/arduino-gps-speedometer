// DISPLAY INCLUDE
#include <Wire.h>
#include "ASOLED.h"
#include "digits.c"

// GPS INCLUDE
#include <UBX_GPS.h>

// GPS VARIABLES
SoftwareSerial gps_serial(4,3);
UBX_GPS GPS(&gps_serial);

//#define __DEBUG__

// GLOBAL VARIABLES
int led_state;
int antenna_state;
int data_displayed;
long prev_speed;

// EXTRA VARIABLES
int extra_state;
bool extra_displayed;
unsigned long last_change;
int GasPin     = 7;  
int TurnPin    = 8;  
int NeutralPin = 9;  

// TIME VARIABLES
int prev_mins;

// ArduLogic VARIABLES
float time_record_30;  // Time to accelerate to 30
float time_record_60;  // Time to accelerate to 60
float time_record_100; // Time to accelerate to 100
unsigned long time_record_start;     // Time from start record
unsigned long time_record_displayed; // Time from start display
int record_displayed; // 0 - not need to displayed
                      // 1 - need to displayed
                      // 2 - already displayed

#ifdef __DEBUG__
int temp_speed;
#endif

void setup()
{  
  // GLOBAL VARIABLES SETUP
  pinMode(LED_BUILTIN, OUTPUT);
  led_state = HIGH;
  antenna_state = 0;
  data_displayed = false;
  prev_speed = 0;

  //EXTRA VARIABLES
  extra_displayed = false;
  extra_state = 0;
  last_change = 0;
  pinMode(GasPin, INPUT); 
  pinMode(TurnPin, INPUT); 
  pinMode(NeutralPin, INPUT); 
  
  // TIME VARIABLES
  prev_mins = 99;
  
  // GPS SETUP
  gps_serial.begin(38400);
  
  // DISPLAY SETUP
  LD.init();  //initialze OLED display
  LD.drawBitmap(Ducati, 0, 0, 128, 8);
  #ifndef __DEBUG__
  delay(2500);
  #endif
  LD.clearDisplay();
  
  // ArduLogic
  time_record_30 = 0;
  time_record_60 = 0;
  time_record_100 = 0;
  time_record_start = 0;
  time_record_displayed = 0;
  record_displayed = false;
  #ifdef __DEBUG__
  temp_speed = 0;
  #endif
}


void loop()
{
  if (GPS.processGPS() ) 
  {          
    digitalWrite(LED_BUILTIN, led_state);
    led_state = !led_state;    

    #ifdef __DEBUG__
    if(!GPS.pvt.fixType)
    #else
    if(GPS.pvt.fixType)
    #endif
    {
      GPS.pvt.gSpeed = (GPS.pvt.gSpeed * 3600) / 1000000; //(GPS.pvt.gSpeed  * 60 * 60) / (1000 * 1000) mm/s to km/h
      #ifdef __DEBUG__
      temp_speed > 150 | temp_speed < 0 ? temp_speed = 0 : temp_speed = temp_speed + rand() % 20 - rand() % 10;
      GPS.pvt.gspeed = temp_speed * 1000;
      #endif
      CheckPins(); // Проверяем пины
      CheckTime(); // Проверяем время разгона
      
      if(!data_displayed)
        LD.clearDisplay();

      DisplaySpeed();
      
      if(prev_mins != GPS.pvt.minute)
      {
        DisplayTime();
        prev_mins = GPS.pvt.minute;
      }
      
      if(record_displayed)
      {
        DisplayRecord();
      }
      else
      {
        if(extra_state)
        {
          DisplayExtra();
        }
        else
        {
          DisplayInfo();
        }
      }

      DisplayExtra();

      data_displayed = true;
    }
    else
    {
      if(data_displayed)
        LD.clearDisplay();
        
      LD.drawBitmap(Antenna[antenna_state % 4], 32, 0, 64, 8);
      antenna_state++;

      data_displayed = false;
      prev_mins = 100;
    }
  }
}

void CheckTime()
{
  if(long(GPS.pvt.gSpeed) == 0)
  {
    if(record_displayed > 0)
    {
        LD.drawBitmap(Extra[0], 83, 2, 45, 6);
    }
    
    time_record_start = millis();
    record_displayed = 0;
    
    time_record_30  = 0;
    time_record_60  = 0;
    time_record_100 = 0;
  }
  else if((long(GPS.pvt.gSpeed) >= 30) && (time_record_30 == 0))
  {
    time_record_30 = float(millis() - time_record_start) / 1000;
    record_displayed = 1;
    time_record_displayed = millis();
  } 
  else if((long(GPS.pvt.gSpeed) >= 60) && (time_record_60 == 0))
  {
    time_record_60 = float(millis() - time_record_start) / 1000;
    record_displayed = 1;
    time_record_displayed = millis();
  } 
  else if((long(GPS.pvt.gSpeed) >= 100) && (time_record_100 == 0))
  {
    time_record_100 = float(millis() - time_record_start) / 1000;
    record_displayed = 1;
    time_record_displayed = millis();
  }
  
  if(record_displayed == 2)
  {
    if(millis() - time_record_displayed > 10000)
    {      
      record_displayed = 0;
      LD.drawBitmap(Extra[0], 83, 2, 45, 6);
      
      //LD.printString_12x16("      ", 80, 2);
      //LD.printString_12x16("      ", 80, 4);
      //LD.printString_12x16("      ", 80, 6);
    }
  }
}

void CheckPins()
{
  if(digitalRead(NeutralPin)) // 9
  {
    extra_state = 3;
    return;
  }
  else
    if(digitalRead(TurnPin)) // 8
    {
      extra_state = 2;
      return;
    }
    else
      if(digitalRead(GasPin)) // 7
      {
        extra_state = 1;
        return;
      }

  extra_state = 0;
}

void DisplayExtra()
{
  if(extra_displayed)
    LD.drawBitmap(Extra[extra_state], 83, 2, 45, 6);
  else
    LD.drawBitmap(Extra[0], 83, 2, 45, 6);
  
  if(millis() - last_change > 500)
  {
    extra_displayed = !extra_displayed;
    last_change = millis();
  }
  
  //extra_state++;
}

void DisplayInfo()
{  
  char sz[8];
  sprintf(sz, " %02d", GPS.pvt.second);
  LD.printString_12x16(sz, 91, 2);
  sprintf(sz, "%3d", GPS.pvt.numSV);
  LD.printString_12x16(sz, 91, 4);
  sprintf(sz, "%3d", long(GPS.pvt.hAcc/1000));
  LD.printString_12x16(sz, 91, 6);
}

void DisplayRecord()
{
  if(record_displayed == 1)
  {
    LD.drawBitmap(Extra[0], 83, 2, 45, 6);
      
    if(time_record_30 != 0)
      PrintRecordTime(time_record_30, 85, 2);
      
    if(time_record_60 != 0)
      PrintRecordTime(time_record_60, 85, 4);
      
    if(time_record_100 != 0)
      PrintRecordTime(time_record_100, 85, 6);

    record_displayed = 2;
  }
}

void PrintRecordTime(float record, int x, int y)
{
  if(record < 10)
  {
    LD.printString_12x16(".", x + 22, y);
    LD.printNumber(long(record), x + 12, y);
    LD.printNumber(long(record*10)%10, x + 30, y);
  }
  else if(record < 100)
  {
    LD.printString_12x16(".", x + 22, y);
    LD.printNumber(long(record), x, y);
    LD.printNumber(long(record*10)%10, x + 30, y);
  }
  else if(record < 1000)
  {
    LD.printNumber(long(record), x + 9, y);
  }
}

void DisplaySpeed()
{
  long Speed = long(GPS.pvt.gSpeed) % 200;

  if((prev_speed >= 100 && Speed < 100) || (prev_speed < 100 && Speed >= 100) )
  {
    if(Speed > 99)
      LD.drawBitmap(digits_slim, 0, 2, 8, 6);
    else
      LD.drawBitmap(digits_slim_empty, 0, 2, 8, 6);
  }
  
  LD.drawBitmap(digits[(Speed/10)%10], 11, 2, 33, 6);
  LD.drawBitmap(digits[ Speed    %10], 47, 2, 33, 6);
  
  prev_speed = Speed;
}

void DisplayTime()
{
  // DATE
  LD.printString_12x16(".", 22, 0);
  GPS.pvt.day = (GPS.pvt.hour >= 21) ? GPS.pvt.day + 1 : GPS.pvt.day;
  
  if(GPS.pvt.day >= 10)
  {
    LD.printNumber(long(GPS.pvt.day), 0, 0);
  }
  else
  {
    LD.printNumber(long(0), 0, 0);
    LD.printNumber(long(GPS.pvt.day), 12, 0);
  }
    
  if(GPS.pvt.month >= 10)
  {
    LD.printNumber(long(GPS.pvt.month), 29, 0);
  }
  else
  {
    LD.printNumber(long(0), 29, 0);
    LD.printNumber(long(GPS.pvt.month), 41, 0);
  }

  // TIME
  int x = 74;
  LD.printString_12x16(":", 22 + x, 0);
  GPS.pvt.hour = (GPS.pvt.hour + 3)%24;

  if(GPS.pvt.hour >= 10)
  {
    LD.printNumber(long(GPS.pvt.hour), 0 + x, 0);
  }
  else
  {
    LD.printNumber(long(0), 0 + x, 0);
    LD.printNumber(long(GPS.pvt.hour), 12 + x, 0);
  }
    
  if(GPS.pvt.minute >= 10)
  {
    LD.printNumber(long(GPS.pvt.minute), 29 + x, 0);
  }
  else
  {
    LD.printNumber(long(0), 29 + x, 0);
    LD.printNumber(long(GPS.pvt.minute), 41 + x, 0);
  }
}

